/*jshint esversion: 8 */
import Vue from 'vue';
import Vuex from 'vuex';
import {localStorageX} from "storagex-js";
import {objectDeepCompare} from '@/common/OtherTools';

Vue.use(Vuex);
const state = localStorageX(
    "StateContainer",
    {
        likeRecord:{  //like记录 已经点过like的
            color:{},
            gradient:{},
            shadow:{},
        },
        myLike:{ //记录我的like
            color:[],
            gradient:[],
            shadow:[],
        },
        myCreate:{ //记录我创作的相关配色
            color:[],
            gradient:[],
            shadow:[],
        },
        theme:{
            background:null,  //背景颜色（css）
            color:null, //字体颜色
        },
    }
);
export default new Vuex.Store({
    state: state,
    mutations: {
        addLikeRecord(state,payload){  //累加like数量的方法 参数{type,id}
            const likeRecord = state.likeRecord;
            let type = payload.type;
            let id = payload.id;
            if(!id || !type) return;
            if(likeRecord[type][id]) return;
            likeRecord[type][id] = true;
        },
        addMyLike(state,payload){ //添加一个我的like 参数{type,data}
            const myLike = state.myLike;
            let type = payload.type;
            let data = payload.data;
            if(!data || !type) return;
            let dataList = myLike[type];
            let _add = false;
            dataList.some(item=>{
                if(item.id == data.id){
                    _add = true;
                    return true;
                }
            });
            if(!_add){  //表示可以添加
                dataList.push(data);
            }else{ //表示已经有该数据了，清除
                myLike[type] = dataList.filter(item=>{
                    return item.id != data.id;
                });
            }
        },
        deleteMyLike(state,payload){ //删除一个我的like 参数{type,id}
            const myLike = state.myLike;
            let type = payload.type;
            let id = payload.id;
            if(!id || !type) return;
            myLike[type] = myLike[type].filter(item=>{
                return item.id != id;
            });
        },
        addMyCreate(state,payload){ //创作一个配色 参数{type,data}
            const myCreate = state.myCreate;
            let type = payload.type;
            let data = payload.data;
            let dataList = myCreate[type];
            let _add = false;
            dataList.some(item=>{
                if(objectDeepCompare(item,data)){
                    _add = true;
                    return true;
                }
            });
            if(_add) return;
            data = JSON.parse(JSON.stringify(data));
            dataList.push(data);
        },
        deleteMyCreate(state,payload){ //删除一个我创作的配色 参数{type,data}
            const myCreate = state.myCreate;
            let type = payload.type;
            let data = payload.data;
            myCreate[type] = myCreate[type].filter(item=>{
                return !objectDeepCompare(item,data);
            });
        },
        setTheme(state,payload){  //设置一个我喜欢的主题 参数{background:data,color:data}
            for(let a in payload){
                if(state.theme[a] !== payload[a]){
                    state.theme[a] = payload[a];
                }else{
                    state.theme[a] = null;
                }
            }
        },
        removeTheme(state){ //清除主题
            state.theme = {
                background:null,  //背景颜色（css）
                color:null, //字体颜色
            }
        },
    },
    getters: {
        getMyLikeState: (state) => (params) => {  //判断我是否添加了like 参数{type,id}
            const myLike = state.myLike;
            let type = params.type;
            let id = params.id;
            let dataList = myLike[type];
            let _state = false;
            dataList.some(item=>{
                if(item.id == id){
                    _state = true;
                    return true;
                }
            });
            return _state;
        }
    },
});
