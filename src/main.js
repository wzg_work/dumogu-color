/*jshint esversion: 8 */
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

//引入js cssvar
import '@/common/CssVar';

//引入公共css
import '@/styles/index.scss';
import '@/styles/cssVar.scss';
import '@/styles/ElementUi.scss';

//按需引入elementUI
import {
  Upload,Button,Input,Popover,Form,
  FormItem,Icon,ColorPicker,Dropdown,
  DropdownMenu,DropdownItem,Pagination,
  Notification,Message,
} from 'element-ui';
Vue.use(Upload);
Vue.use(Button);
Vue.use(Input);
Vue.use(Popover);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Icon);
Vue.use(ColorPicker);
Vue.use(Dropdown);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Pagination);
Vue.prototype.$notify = Notification;
Vue.prototype.$message = Message;

//引入Api，const
import {Api,Const} from "@/common/http";
Vue.prototype.$Api = Api;
Vue.prototype.$Const = Const;

//引入一键复制
import VueClipboard from 'vue-clipboard2';
Vue.use(VueClipboard);  //用于一键复制内容到粘贴板的方法 

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
