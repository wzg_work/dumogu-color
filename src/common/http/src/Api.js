/*jshint esversion: 9 */
import {
    service,
} from "./Request";
import {
    formatURLSearchParams,
} from './Tools';
/*
 配色
 */
function getColorList(params){
    return service({
        url:'/getColorList',
        method:'post',
        params:formatURLSearchParams(params),
    })
}
function getColor(params){
    return service({
        url:'/getColor',
        method:'post',
        params:formatURLSearchParams(params),
    })
}
function addColorLike(params){
    return service({
        url:'/addColorLike',
        method:'post',
        params:formatURLSearchParams(params),
    })
}
/*
 渐变色
 */
 function getGradients(params){
    return service({
        url:'/getGradients',
        method:'post',
        params:formatURLSearchParams(params),
    })
}
function getGradient(params){
    return service({
        url:'/getGradient',
        method:'post',
        params:formatURLSearchParams(params),
    })
}
function addGradientLike(params){
    return service({
        url:'/addGradientLike',
        method:'post',
        params:formatURLSearchParams(params),
    })
}
/*
 边框阴影
 */
 function getShadows(params){
    return service({
        url:'/getShadows',
        method:'post',
        params:formatURLSearchParams(params),
    })
}
function getShadow(params){
    return service({
        url:'/getShadow',
        method:'post',
        params:formatURLSearchParams(params),
    })
}
function addShadowLike(params){
    return service({
        url:'/addShadowLike',
        method:'post',
        params:formatURLSearchParams(params),
    })
}
/*
 其他
 */
function upload(params){
    return service({
        url:'/upload',
        method:'post',
        data:params,
    })
}
function search(params){
    return service({
        url:'/search',
        method:'post',
        data:params,
    })
}

export default{
    getColorList,
    getColor,
    addColorLike,
    getGradients,
    getGradient,
    addGradientLike,
    getShadows,
    getShadow,
    addShadowLike,
    upload,
    search,
};