/*jshint esversion: 9 */
import {isProdoction} from './Tools';
const Timeout = 30000;  //请求超时时间
let baseApiURL;  //api原始链接
if(isProdoction()){  //development 为开发环境，production 为生产环境
    baseApiURL = 'https://api.dumogu.top/color';
}else{
    baseApiURL = 'http://127.0.0.1:8000/color';
}
export default{
    Timeout,
    baseApiURL,
};