/*jshint esversion: 9 */
/*
 css全局变量工具，一篇文字
 */
 import {TaskControl} from './SimpleAnimation';

 const clientHeight = '--clientHeight'; //屏幕可用区域高度
 
 const taskServe = new TaskControl();
 taskServe.add(()=>{
     let clientHeight_ = `${document.documentElement.clientHeight}px`;
     if (document.documentElement.style.getPropertyValue(clientHeight) != clientHeight_) {
         document.documentElement.style.setProperty(
             clientHeight,
             clientHeight_,
         ); //可见区域高度  css全局变量
     }
 });