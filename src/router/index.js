/*jshint esversion: 8 */
import Vue from 'vue';
import VueRouter from 'vue-router';

import indexLayout from '@/layout/index';
import notfount from "@/views/notFount.vue";
import Home from "@/views/home";
import Mine from "@/views/mine";
import Search from "@/views/search";
import Create from "@/views/create";
import ColorTaking from "@/views/colorTaking";
import Color from "@/views/color";
import ColorInfo from "@/views/colorInfo";
import Gradient from "@/views/gradient";
import GradientInfo from "@/views/gradientInfo";
import Shadow from "@/views/shadow";
import ShadowInfo from "@/views/shadowInfo";
import CreateColor from "@/views/create/components/CreateColor";
import CreateGradient from "@/views/create/components/CreateGradient";
import CreateShadow from "@/views/create/components/CreateShadow";

Vue.use(VueRouter);
const routes = [
    {
        path: '*',  //404页面
        name: 'Notfount',
        component: notfount,
        meta: {
            title: '没找到该页面',
            myKeepAlive:true,
        },
    },
    {
        path: '/',
        name:'IndexLayout',
        component: indexLayout,
        redirect: 'Home',
        meta: {
            myKeepAlive:true,
        },
        children: [
            { 
                path: '',
                name:'Home',
                component: Home,
                meta: {
                    myKeepAlive:true,
                    title: '首页',
                },
            },
            {
                path: '/taking',
                name:'Taking',
                component: ColorTaking,
                meta: {
                    myKeepAlive:true,
                    title: '颜色采取',
                },
            },
            {
                path: '/color',
                name:'Color',
                component: Color,
                meta: {
                    myKeepAlive:true,
                    title: '颜色',
                },
            },
            {
                path: '/color/:id',
                name:'ColorInfo',
                component: ColorInfo,
                meta: {
                    myKeepAlive:true,
                    title: '颜色配色详情',
                },
            },
            {
                path: '/gradient',
                name:'Gradient',
                component: Gradient,
                meta: {
                    myKeepAlive:true,
                    title: '渐变色页',
                },
            },
            {
                path: '/gradient/:id',
                name:'GradientInfo',
                component: GradientInfo,
                meta: {
                    myKeepAlive:true,
                    title: '渐变色详情',
                },
            },
            { 
                path: '/shadow',
                name:'Shadow',
                component: Shadow,
                meta: {
                    myKeepAlive:true,
                    title: '边框阴影',
                },
            },
            {
                path: '/shadow/:id',
                name:'ShadowInfo',
                component: ShadowInfo,
                meta: {
                    myKeepAlive:true,
                    title: '边框阴影详情',
                },
            },
            {
                path: '/search',
                name:'Search',
                component: Search,
                meta: {
                    myKeepAlive:true,
                    title: '搜索',
                },
            },
            { 
                path: '/create',
                name:'Create',
                component: Create,
                meta: {
                    myKeepAlive:true,
                    title: '创造',
                },
                children: [
                    {
                        path: '',
                        name:'Create',
                        redirect: 'color',
                    },
                    {
                        path: 'color',
                        name:'Create',
                        component: CreateColor,
                    },
                    {
                        path: 'gradient',
                        name:'Create',
                        component: CreateGradient,
                    },
                    {
                        path: 'shadow',
                        name:'Create',
                        component: CreateShadow,
                    },
                ],
            },
            {
                path: '/mine',
                name:'Mine',
                component: Mine,
                meta: {
                    myKeepAlive:true,
                    title: '我的',
                },
            },
        ],
    },
];

const router = new VueRouter({
    mode: 'history',
    routes,
});

export default router;
