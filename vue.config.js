/*jshint esversion: 8 */
// process.env.NODE_ENV = 'production';
const isProduction = process.env.NODE_ENV === 'production';
const path = require('path');
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const resolve = dir => {
    return path.join(__dirname, dir);
};
module.exports = {
    /* 部署生产环境和开发环境下的URL：可对当前环境进行区分，baseUrl 从 Vue CLI 3.3 起已弃用，要使用publicPath */
    publicPath: isProduction?
        'https://dumogu.oss-cn-chengdu.aliyuncs.com/color/':'/',
    /* 输出文件目录：在npm run build时，生成文件的目录名称 */
    outputDir: 'dist',
    /* 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录 */
    assetsDir: "static",
    /* 是否在构建生产包时生成 sourceMap 文件，false将提高构建速度 */
    productionSourceMap: false,
    /* 默认情况下，生成的静态资源在它们的文件名中包含了 hash 以便更好的控制缓存，你可以通过将这个选项设为 false 来关闭文件名哈希。(false的时候就是让原来的文件名不改变) */
    filenameHashing: false,
    /* 代码保存时进行eslint检测 */
    lintOnSave: true,
    //webpack配置
    configureWebpack:config => {
        if (isProduction) {
            const WebpackAliyunOss = require('webpack-aliyun-oss');
            const OSSConfig = require('./oss.config');
            // 开启gzip压缩
            config.plugins.push(new CompressionWebpackPlugin({
                algorithm: 'gzip',
                test: /\.js$|\.html$|\.json$|\.css/,
                threshold: 10240,
                minRatio: 0.8,
                deleteOriginalAssets: false, //是否删除原文件
            }));
            // 取消webpack警告的性能提示
            config.performance = {
                hints:'warning',
                    //入口起点的最大体积
                    maxEntrypointSize: 50000000,
                    //生成文件的最大体积
                    maxAssetSize: 30000000,
                    //只给出 js 文件的性能提示
                    assetFilter: function(assetFilename) {
                return assetFilename.endsWith('.js');
                }
            };
            config.plugins.push(
                new WebpackAliyunOss({
                    from: ['./dist/**', '!./dist/**/*.html'], // 上传那个文件或文件夹  可以是字符串或数组
                    dist: "/color",  // 需要上传到oss上的给定文件目录
                    region: OSSConfig.region,
                    accessKeyId: OSSConfig.accessKeyId,
                    accessKeySecret: OSSConfig.accessKeySecret,
                    bucket: OSSConfig.bucket,
                    test: !isProduction,//可以在进行测试看上传路径是否正确, 打开后只会显示上传路径并不会真正上传;
                    // 因为文件标识符 "\"  和 "/" 的区别 不进行 setOssPath配置,上传的文件夹就会拼到文件名上, 丢失了文件目录,所以需要对setOssPath 配置。
                    setOssPath: filePath => {
                        // some operations to filePath
                        let index = filePath.lastIndexOf("dist");
                        let Path = filePath.substring(index + 4, filePath.length);
                        return Path.replace(/\\/g, "/");
                    },
                    setHeaders: filePath => {
                        return {
                            "Cache-Control": "max-age=31536000"
                        };
                    }
                }),
            );
        }
    },
    devServer: {
        host: 'localhost',
        port: 8080, // 端口号 
        hotOnly: false,
        https: false, // https:{type:Boolean}
        open: false, //配置自动启动浏览器
        proxy:null // 配置跨域处理,只有一个代理
    },
    chainWebpack: config => {
        config.resolve.alias
        .set('@', resolve('src')) // key,value自行定义，比如.set('@@', resolve('src/components'))
        .set('@c', resolve('src/components'));
        config.plugins.delete('prefetch');
        config.plugins.delete('preload-search');
        if(isProduction){ //如果是开发环境才执行
            //解决打包head标签里属性没有双引号 仅限于线上
            config.plugin('html')
            .tap(args => {
                args[0].minify.removeAttributeQuotes = false;
                return args;
            });
        }
        
    },
};
